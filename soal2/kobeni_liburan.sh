#!/bin/bash

folders=$(find -type d -name "*kumpulan*")
zips=$(find -name "*.zip" | grep -i devil)
kumpulan_index=1
zip_index=1
folder_name=""
current_hour=$(date +%H)
IMAGE_URL="https://source.unsplash.com/random/800x600/?indonesia"

function find_index() {
    if [ -z "$folders" ];then
        return
    fi

    for folder in $folders; do
        ((kumpulan_index++))
    done
    return
}

function create_kumpulan() {
    folder_name="kumpulan_$kumpulan_index"
    mkdir -p "$folder_name"
}

function download_img() {
    if [ $current_hour -eq 0 ]
    then current_hour=1
    fi

    for i in $(seq $current_hour)
    do
        wget -O "$folder_name/perjalanan_$i.jpg" "$IMAGE_URL"
    done
}

function find_index_zip() {
    if [ -z "$zips" ];then
        return
    fi

    for zip in $zips; do
        ((zip_index++))
    done
    return
}

function execute() {
    find_index
    create_kumpulan
    download_img
}

find_index
create_kumpulan
download_img

# Cronjob untuk Penggunaan script setiap 10 Jam
# echo "0 */10 * * * /home/ASUS/Desktop/Linux/sisop-praktikum-modul-1-2023-u02/Soal2/kobeni_liburan.sh" | crontab -

# Cronjob untuk Penggunaan script setiap 1 hari jam 00.00
# echo "0 0 * * * cd /home/ASUS/Desktop/Linux/sisop-praktikum-modul-1-2023-u02/Soal2 && zip -r devil_$(date +\%Y\%m\%d).zip kumpulan_* && rm -rf kumpulan_*" | crontab -
