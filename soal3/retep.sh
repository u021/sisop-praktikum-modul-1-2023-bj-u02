#!/bin/bash

USERS_FILE="users/users.txt"
LOG_FILE="log.txt"

if [ ! -f $USERS_FILE ]; then
  echo "No registered users recorded!"
fi

function login_user {
    read -p "Enter username: " username
    read -p "Enter password: " password

  if grep -q "^$username" $USERS_FILE; then
    if grep -q "$username:$password" $USERS_FILE; then
      echo "Login Success!"
      echo "$(date "+%y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" >> $LOG_FILE
    else
      echo "Wrong Password!"
      echo "$(date "+%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> $LOG_FILE
    fi
  else
    echo "No account with username $username!"
  fi
}

login_user