#!/bin/bash

USERS_FILE="users/users.txt"
LOG_FILE="log.txt"

if [ ! -f $USERS_FILE ]; then
  mkdir -p "users"
  touch $USERS_FILE
fi

if [ ! -f $LOG_FILE ]; then
  touch $LOG_FILE
fi

function is_alphanumeric {
  if [[ "$1" =~ [^a-zA-Z0-9] ]]; then
    return 1
  else
    return 0
  fi
}

function has_uppercase {
  if [[ "$1" =~ [A-Z] ]]; then
    return 0
  else
    return 1
  fi
}

function has_lowercase {
  if [[ "$1" =~ [a-z] ]]; then
    return 0
  else
    return 1
  fi
}

function register_user {
  read -p "Enter username: " username
  
  if grep -q "^$username" $USERS_FILE; then
    echo "$(date "+%y/%m/%d %H:%M:%S") REGISTER: ERROR User $username already exists." >> $LOG_FILE
    echo "User $username already exists!"
    return 1
  fi
  
  while true; do
    read -p "Enter password: " password
    
    if [[ ${#password} -ge 8 ]] && has_uppercase $password && has_lowercase $password && is_alphanumeric $password && [[ $password != "$username" ]] && [[ $password != *"chicken"* ]] && [[ $password != *"ernie"* ]]; then
      echo "$username:$password" >> $USERS_FILE
      echo "$(date "+%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully." >> $LOG_FILE
      echo "User $username is registered!"
      break
    else
      echo "Password does not meet the requirements. Please try again."
    fi
  done
}

register_user
