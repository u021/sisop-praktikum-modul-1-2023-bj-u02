#!/bin/bash

# Define input and output file paths
input="$(date +"%H:%M_%d:%m:%Y").txt"
decrypted_output="$(date +"%H:%M_%d:%m:%Y")_decrypted.txt"

# Define encryption characters and shift value
char=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
enc=$(( $(date +%H) % 26 ))

# Decrypt output file and save to new file
tr "${char:${enc}:26}${char:${enc}:26}" "${char:0:26}${char:26:${#char}}" < "$input" > "$decrypted_output"
