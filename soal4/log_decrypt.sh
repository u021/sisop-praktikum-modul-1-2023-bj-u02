#!/bin/bash

# Define input and output file paths
input="/var/log/syslog"
output="$(date +"%H:%M_%d:%m:%Y").txt"

# Define encryption characters and shift value
char=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
enc=$(( $(date +%H) % 26 ))

# Encrypt input file using the Caesar cipher and save to output file
tr "${char:0:26}${char:26:${#char}}" "${char:${enc}:26}${char:${enc}:26}" < "$input" > "$output"
