#! /bin/bash

CSV_FILE="2023 QS World University Rankings.csv"
TOP5=$(awk -F',' '$3 == "JP" {print}' "$CSV_FILE" | sort -t',' -k1n | head -n 5)

echo "5 Universitas dengan ranking tertinggi di Jepang: "
echo "$TOP5" | awk -F',' '{print $2}'

echo ""
echo "Faculty Student Score (FSR) Terendah adalah: "
UNIV=$(echo "$TOP5" | sort -t',' -k9n | head -n 1)
echo $UNIV | awk -F',' '{print $2}'

echo ""
echo "10 Universitas di Jepang dengan GER-rank tertinggi: "
TOP10=$(awk -F',' '$3 == "JP" {print}' "$CSV_FILE" | sort -t',' -k20n | head -n 10)
echo "$TOP10" | awk -F',' '{print $2}'

echo ""
echo "Universitas 'keren' : "
KEREN=$(awk -F',' 'tolower($2) ~ /keren/ {print}' "$CSV_FILE")
echo "$KEREN" | awk -F',' '{print $2}'
