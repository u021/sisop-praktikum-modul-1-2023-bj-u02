<h1>Praktikum Modul 1 Sistem Operasi</h1>
<h3>Group U02</h3>

| NAME                      | NRP       |
|---------------------------|-----------|
|Pascal Roger Junior Tauran |5025211072 |
|Riski Ilyas                |5025211189 |
|Armstrong Roosevelt Zamzami|5025211191 |


## Number 1
```
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
```
### 1A

```
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
```
Solution:<br>
```sh
CSV_FILE="2023 QS World University Rankings.csv"
TOP5=$(awk -F',' '$3 == "JP" {print}' "$CSV_FILE" | sort -t',' -k1n | head -n 5)
echo "5 Universitas dengan ranking tertinggi di Jepang: "
echo "$TOP5" | awk -F',' '{print $2}'
```
Exlpanation:
- `awk -F',' '$3 == "JP" {print}' "$CSV_FILE"` searches for rows that have "JP" in the third column in the "$CSV_FILE" and prints them.

### 1B
```
Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.
```
Solution:<br>
```sh
echo ""
echo "Faculty Student Score (FSR) Terendah adalah: "
UNIV=$(echo "$TOP5" | sort -t',' -k9n | head -n 1)
echo $UNIV | awk -F',' '{print $2}'
```
Explanation:
- `sort -t',' -k9n` sorts the rows based on the 9th column in numerical order.
- `head -n 1` selects the first row of the sorted output which is the university with the lowest FSR.
### 1C
```
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```
Solution:<br>
```sh
echo ""
echo "10 Universitas di Jepang dengan GER-rank tertinggi: "
TOP10=$(awk -F',' '$3 == "JP" {print}' "$CSV_FILE" | sort -t',' -k20n | head -n 10)
echo "$TOP10" | awk -F',' '{print $2}'
```
Explanation:
- `awk -F',' '$3 == "JP" {print}' "$CSV_FILE"` searches for rows that have "JP" in the third column in the "$CSV_FILE" and prints them.
- `sort -t',' -k20n` sorts the output based on the 20th column in numerical order.
- `head -n 10` selects the top 10 rows to be printed.
### 1D
```
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.
```
Solution:<br>
```sh
echo ""
echo "Universitas 'keren' : "
KEREN=$(awk -F',' 'tolower($2) ~ /keren/ {print}' "$CSV_FILE")
echo "$KEREN" | awk -F',' '{print $2}'
```
Explanation:
- `'tolower($2) ~ /keren/ {print}'` searches for rows that have "keren" in uppercase or lowercase. The resulting lines are printed


Link video revisi nomer1:

- `https://www.youtube.com/watch?v=AUILnPEhg1k`

##  Number2
```
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
```
### 2A
```
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
```
Solution:<br>
```sh
#!/bin/bash

folders=$(find -type d -name "*kumpulan*")
zips=$(find -name "*.zip" | grep -i devil)
kumpulan_index=1
zip_index=1
folder_name=""
current_hour=$(date +%H)
IMAGE_URL="https://source.unsplash.com/random/800x600/?indonesia"

function find_index() {
    if [ -z "$folders" ];then
        return
    fi

    for folder in $folders; do
        ((kumpulan_index++))
    done
    return
}

function create_kumpulan() {
    folder_name="kumpulan_$kumpulan_index"
    mkdir -p "$folder_name"
}

function download_img() {
    if [ $current_hour -eq 0 ]
    then current_hour=1
    fi

    for i in $(seq $current_hour)
    do
        wget -O "$folder_name/perjalanan_$i.jpg" "$IMAGE_URL"
    done
}

function find_index_zip() {
    if [ -z "$zips" ];then
        return
    fi

    for zip in $zips; do
        ((zip_index++))
    done
    return
}

function execute() {
    find_index
    create_kumpulan
    download_img
}

function zip() {
    folders=$(find -type d -name "*kumpulan*")
    find_index_zip
    zip -r devil_$zip_index.zip $folders
}

find_index
create_kumpulan
download_img

# Cronjob untuk Penggunaan script setiap 10 Jam
# echo "0 */10 * * * /home/ASUS/Desktop/Linux/sisop-praktikum-modul-1-2023-u02/Soal2/kobeni_liburan.sh" | crontab -

```
Explanation:
- `find_index` function increments the "kumpulan_index" based on the number of existing "kumpulan" folders.
- `create_kumpulan` function creates a new "kumpulan_x" folder (x being the next availabe index) using `mkdir` command.
- `download_img` function downloads copies of an image based on the hour of the day. `wget` is used to name the folder as "kumpulan_x". (x being the current index)
- `find_index_zip` increments "zip_index" based on the number devil zip folders.
- `execute` runs "find_index", "create_kumpulan" and "download_img".
- `zip` creates a new "devil_x.zip" zip file using the `zip` command. (x being the current index) 
### 2B
```
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.
```
Solution:<br>
```sh

# Cronjob untuk zip setiap 1 hari
# echo "0 0 * * * cd /home/ASUS/Desktop/Linux/sisop-praktikum-modul-1-2023-u02/Soal2 && zip -r devil_$(date +\%Y\%m\%d).zip kumpulan_* && rm -rf kumpulan_*" | crontab -

```

Explanation:

## Number3
```
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
```
## 3A
```
Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
Minimal 8 karakter
Memiliki minimal 1 huruf kapital dan 1 huruf kecil
Alphanumeric
Tidak boleh sama dengan username 
Tidak boleh menggunakan kata chicken atau ernie
```
Solution:<br>

```sh

function is_alphanumeric {
  if [[ "$1" =~ [^a-zA-Z0-9] ]]; then
    return 1
  else
    return 0
  fi
}

function has_uppercase {
  if [[ "$1" =~ [A-Z] ]]; then
    return 0
  else
    return 1
  fi
}

function has_lowercase {
  if [[ "$1" =~ [a-z] ]]; then
    return 0
  else
    return 1
  fi
}

function register_user {
  read -p "Enter username: " username
  
  if grep -q "^$username" $USERS_FILE; then
    echo "$(date "+%y/%m/%d %H:%M:%S") REGISTER: ERROR User $username already exists." >> $LOG_FILE
    echo "User $username already exists!"
    return 1
  fi
  
  while true; do
    read -p "Enter password: " password
    
    if [[ ${#password} -ge 8 ]] && has_uppercase $password && has_lowercase $password && is_alphanumeric $password && [[ $password != "$username" ]] && [[ $password != *"chicken"* ]] && [[ $password != *"ernie"* ]]; then
      echo "$username:$password" >> $USERS_FILE
      echo "$(date "+%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully." >> $LOG_FILE
      echo "User $username is registered!"
      break
    else
      echo "Password does not meet the requirements. Please try again."
    fi
  done
}

```

Explanation:
- `is_alphanumeric` function checks whether a string contains only alphanumeric characters and returns "1" if it is true and "0" if it is false.
- `has_uppercase` checks whether a string has at least 1 uppercase letter and returns "1" if it is true and "0" otherwise.
- `has_lowercase` checks whether a string contains at least 1 lowercase letter and returns "1" if it does and "0" otherwise.
- `register_user` is the main function of the script. It promts the user to enter a username and crosschecks the `user.txt` file whether or not the username already exists in the file. It will log an error message to the `log.txt` file if the user already exists in the `user.txt` file. The function will then promt the user to enter a password that matches the requirements set by `is_alphanumeric`, `has_uppercase` and `has_lowercase`.  This will then enter a loop until the user enters a password that matches the requirements. Once this is done, the function adds the username and password to the `users.txt` file and logs a sucess message to the `log.txt` file.

## 3B
```
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
```
Solution:<br>

louis.sh
```sh

function register_user {
  read -p "Enter username: " username
  
  if grep -q "^$username" $USERS_FILE; then
    echo "$(date "+%y/%m/%d %H:%M:%S") REGISTER: ERROR User $username already exists." >> $LOG_FILE
    echo "User $username already exists!"
    return 1
  fi
  
  while true; do
    read -p "Enter password: " password
    
    if [[ ${#password} -ge 8 ]] && has_uppercase $password && has_lowercase $password && is_alphanumeric $password && [[ $password != "$username" ]] && [[ $password != *"chicken"* ]] && [[ $password != *"ernie"* ]]; then
      echo "$username:$password" >> $USERS_FILE
      echo "$(date "+%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully." >> $LOG_FILE
      echo "User $username is registered!"
      break
    else
      echo "Password does not meet the requirements. Please try again."
    fi
  done
}

```

retep.sh
```sh

function login_user {
    read -p "Enter username: " username
    read -p "Enter password: " password

  if grep -q "^$username" $USERS_FILE; then
    if grep -q "$username:$password" $USERS_FILE; then
      echo "Login Success!"
      echo "$(date "+%y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" >> $LOG_FILE
    else
      echo "Wrong Password!"
      echo "$(date "+%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> $LOG_FILE
    fi
  else
    echo "No account with username $username!"
  fi
}

```

Explanation:<br>
`louis.sh`<br>
- `register_user` is the main function of the script. It promts the user to enter a username `$username` and crosschecks the `user.txt`/`$USERS_FILE` file whether or not the username already exists in the file. It will log an error message to the `log.txt` file if the user already exists in the `user.txt`/`$USERS_FILE` file. The function will then promt the user to enter a password that matches the requirements set by `is_alphanumeric`, `has_uppercase` and `has_lowercase`.  This will then enter a loop until the user enters a password that matches the requirements. Once this is done, the function adds the username and password to the `user.txt`/`$USERS_FILE` file and logs a sucess message to the `log.txt` file.
<br>
<code>retep.sh</code><br>
- <code>login_user</code> is the main function of this script. It promts the user to enter their username <code>$username</code> and checks the<code>user.txt/$USERS_FILE</code> file whether or not the username exsits. If it does not, it will log an error message to the <code>log.txt</code> file. If the entered username exists in the <code>user.txt/$USERS_FILE</code>, it will promt the user to enter enter the password / <code>$username:$password</code> for that username. If the <code>$username:$password</code> is incorrent, it will log an error message to the <code>log.txt</code> file. And if the the <code>$username:$password</code> is correct, it will log a login message in the <code>log.txt</code> file.
 
## Number4
```
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
```
## 4A
```
Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
```
Solution:<br>
```sh
output="$(date +"%H:%M_%d:%m:%Y").txt"
```

Explanation:
- `date +"%H:%M_%d:%m:%Y"Y` this command will set tha name of the `.txt` file to have the format `hour:minute date:month:year.txt`.
## 4B
```
Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
Setelah huruf z akan kembali ke huruf a
```
Solution:<br>
```sh
#!/bin/bash

input="/var/log/syslog"
output="$(date +"%H:%M_%d:%m:%Y").txt"

char=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
enc=$(( $(date +%H) % 26 ))

tr "${char:0:26}${char:26:${#char}}" "${char:${enc}:26}${char:${enc}:26}" < "$input" > "$output"

```

Explanation:
- `char` represents all the characters that are the key for the encryption.
- `enc` gets the value that shifts the key of the encryption.
- `tr` encrypts the `input` and with `enc` based on the characters `char`.
- The output has the file format as `hour:minute date:month:year.txt`.
## 4C 
```
Buat juga script untuk dekripsinya.
```
Solution:<br>
```sh
#!/bin/bash

input="$(date +"%H:%M_%d:%m:%Y").txt"
decrypted_output="$(date +"%H:%M_%d:%m:%Y")_decrypted.txt"

char=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
enc=$(( $(date +%H) % 26 ))

tr "${char:${enc}:26}${char:${enc}:26}" "${char:0:26}${char:26:${#char}}" < "$input" > "$decrypted_output"
```

Explanation:
- `char` represents all the characters that are the key for the decryption.
- `enc` gets the value that shifts the key of the decryption.
- `tr` decrypts the `input` and with `enc` based on the characters `char`.
- The output has the file format as `hour:minute date:month:year_decrypted.txt`
## 4D
```
Backup file syslog setiap 2 jam untuk dikumpulkan 
```
Solution:<br>
```sh
0 */2 * * * /home/borgir/Downloads/encrypt_log.sh
0 */2 * * * /home/borgir/Downloads/decrypt_log.sh
```
Explanation:
- This cron jobs automatically runs `encrypt_log.sh` and `decrypt_log.sh` scripts every 2 hours
